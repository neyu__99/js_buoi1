// 1. Tính lương nhân viên, 100.000/ngày, user nhập số ngày làm
console.log('--ex1')
// input: lương ngày (100.000/ngày), tổng ngày làm (10 ngày)
var LuongNgay=100000;
console.log("LuongNgay =", LuongNgay.toLocaleString(),'VND')
var TongNgay=10;
console.log("TongNgay =", TongNgay,'ngày')
// output: tổng lương
var TongLuong;
// progress 
TongLuong=LuongNgay*TongNgay;
console.log("TongLuong =", TongLuong.toLocaleString(),'VND')


// 2. Tính giá trị trung bình của 5 số thực (user nhập 5 số)
console.log('--ex2')
// input: 5 số
var num1=4;
console.log("num1 =", num1)
var num2=20;
console.log("num2 =", num2)
var num3=37;
console.log("num3 =", num3)
var num4=134;
console.log("num4 =", num4)
var num5=911;
console.log("num5 =", num5)
// output: giá trị trung bình
var Average;
// progress 
Average=(num1+num2+num3+num4+num5)/5
console.log("Average =", Average)


// 3. Quy đổi tiền USD sang VND, tỷ giá 23.500, user nhập USD
console.log('--ex3')
// input: tỷ giá, USD
var TyGia=23500;
console.log("TyGia =", TyGia.toLocaleString())
var USD=99;
console.log("USD =", USD,'$')
// output: VND
var VND;
// progress 
VND=USD*TyGia
console.log("VND =", VND.toLocaleString(),'VND')


// 4. Tính diện tích chu vi HCN, user nhập dài rộng
console.log('--ex4')
// input: Chiều dài, chiều rộng
var ChieuDai=8;
console.log("ChieuDai =", ChieuDai)
var ChieuRong=5;
console.log("ChieuRong =", ChieuRong)
// output: chu vi, diện tích
var ChuVi;
var DienTich;
// progress 
ChuVi=(ChieuDai+ChieuRong)*2
console.log("ChuVi =", ChuVi)
DienTich=ChieuDai*ChieuRong
console.log("DienTich =", DienTich)


// 5. Tính tổng 2 ký số của 1 số, user nhập số
console.log('--ex5')
// input: số
var number=72;
console.log("number =", number)

// output: tổng 2 ký số
var tong;
// progress 
var chuc=Math.floor(number/10)
var donvi=number%10
tong=chuc+donvi
console.log("tong =", tong)

